﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class GPGSBtn : MonoBehaviour
{
    //public Text Login_Label = null;
    public Text User_Label = null;
    public RawImage User_Texture = null;


    public Text Achiv;
    //leaderboard strings
    private string leaderboard = "CgkIkfrUqMkQEAIQBg";
    //achievement strings
    private string achievement1 = "CgkIkfrUqMkQEAIQAQ"; // 화면터치 10000회
    private string achievement2 = "CgkIkfrUqMkQEAIQAg"; // 장비 업그레이드 5회
    private string achievement3 = "CgkIkfrUqMkQEAIQAw"; // 직원 업그레이드 5회
    private string achievement4 = "CgkIkfrUqMkQEAIQBA"; // 황금 벽돌 100개 획득

    private int touchCount = 0;

    void Start()
    {
        GPGSMng.GetInstance.InitializeGPGS(); // 초기화
        GPGSMng.GetInstance.LoginGPGS();
        Social.ShowAchievementsUI();
    }

    //void Update()
    //{
    //    if (GPGSMng.GetInstance.bLogin == false)
    //    {
    //        Login_Label.text = "Login";
    //    }
    //    else
    //    {
    //        Login_Label.text = "Logout";

    //        User_Label.enabled = true;
    //        User_Texture.enabled = true;

    //        User_Label.text = GPGSMng.GetInstance.GetNameGPGS();
    //        User_Texture.texture = GPGSMng.GetInstance.GetImageGPGS();
    //        //SettingUser();
    //    }

        
    //}



    //public void TouchAchiev()
    //{       

    //    if (Social.localUser.authenticated)
    //    {
           
    //        ((PlayGamesPlatform)Social.Active).IncrementAchievement(achievement1, 50, (bool success) =>
    //        {
    //            //Achiv.text = "터치 수 : " + touchCount + "/10";
    //        });

    //        Social.ReportProgress(achievement1, 100f, (bool success) =>
    //        {

    //        });
    //    }
    //}

    public void ShowAchiev()
    {
        Social.ShowAchievementsUI();
    }



    //public void LeaderBoardEvent()
    //{
  
    //    if (Social.localUser.authenticated)
    //    {
    //        Social.ReportScore(5000, leaderboard, (bool success) =>
    //        {
    //            if (success)
    //            {
    //                ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(leaderboard);
    //            }
    //            else
    //            {
    //                //Debug.Log("Login failed for some reason");
    //            }
    //        });
    //    }

    //}

    //public void ShowLeaderBoard()
    //{
    //    if (Social.localUser.authenticated)
    //    {
    //        ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(leaderboard);
    //    }
    //}

    public void ClickEvent()
    {
        if (GPGSMng.GetInstance.bLogin == false)
        {
            GPGSMng.GetInstance.LoginGPGS(); // 로그인
        }
        else
        {
            GPGSMng.GetInstance.LogoutGPGS(); // 로그아웃
        }
    }

    //void SettingUser()
    //{
    //    if (User_Texture != null)
    //        return;

    //    User_Label.enabled = true;
    //    User_Texture.enabled = true;
                
    //    User_Label.text = "UserName : " + GPGSMng.GetInstance.GetNameGPGS();
    //    User_Texture.texture = GPGSMng.GetInstance.GetImageGPGS();
    //}
}