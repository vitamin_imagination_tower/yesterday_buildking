﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Net.Cache;
using System.Text.RegularExpressions;
using UnityEngine.UI; 

static public class ConstantsRW
{
    public static int OBJ_ARRAY_SIZE = 40;
}


public class ReadWriteData : MonoBehaviour
{
    public GameObject m_pPlayer;
    public GameObject m_pEmployeeManager;
    public GameObject m_pEquipmentManager;
    public GameObject m_pStageManger;

    public GameObject m_pNotConnectedRewardText;

    private PlayerInfo m_pPlayerInfo;

    private bool m_bReadComplete = false;

    public static System.DateTime GetNistTime()
    {
        System.DateTime dateTime = System.DateTime.MinValue;

        System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("http://nist.time.gov/actualtime.cgi?lzbc=siqm9b");
        request.Method = "GET";
        request.Accept = "text/html, application/xhtml+xml, */*";
        request.UserAgent = "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)";
        request.ContentType = "application/x-www-form-urlencoded";
        request.CachePolicy = new RequestCachePolicy(RequestCacheLevel.NoCacheNoStore); //No caching
        System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse();
        if (response.StatusCode == System.Net.HttpStatusCode.OK)
        {
            StreamReader stream = new StreamReader(response.GetResponseStream());
            string html = stream.ReadToEnd();//<timestamp time=\"1395772696469995\" delay=\"1395772696469995\"/>
            string time = Regex.Match(html, @"(?<=\btime="")[^""]*").Value;
            double milliseconds = System.Convert.ToInt64(time) / 1000.0;
            dateTime = new System.DateTime(1970, 1, 1).AddMilliseconds(milliseconds).ToLocalTime();
        }

        return dateTime;
    }


    void Start()
    {
        m_pPlayerInfo = new PlayerInfo(0, 0, 0, 0, 0, null, null, 0, GetNistTime(), 0);

        ReadFile();

        m_pPlayer.GetComponent<CPlayer>().SetGold(m_pPlayerInfo.GetGold());
        m_pPlayer.GetComponent<CPlayer>().SetDiamond(m_pPlayerInfo.GetDiamond());
        m_pPlayer.GetComponent<CPlayer>().m_pStageManager.GetComponent<CStageManager>().LoadStage(m_pPlayerInfo.GetCurrentMission(), m_pPlayerInfo.GetCurrentPoint());
        m_pPlayer.GetComponent<CPlayer>().SetPlayerInfo(m_pPlayerInfo);
        m_pEmployeeManager.GetComponent<CEmployeeManager>().SetEmployee(m_pPlayerInfo.GetEmployee());
        m_pEquipmentManager.GetComponent<CEmployeeManager>().SetEqiupment(m_pPlayerInfo.GetEquipment());

        ulong time_tick = (ulong)(GetNistTime() - m_pPlayerInfo.GetLastDateTime()).Ticks / 10000000;
        ulong add_point = (ulong)(m_pPlayer.GetComponent<CPlayer>().GetBPS() * time_tick * 0.1f);
        ulong curr_point = m_pStageManger.GetComponent<CStageManager>().GetCurrentPoint();
        ulong goal_point = m_pStageManger.GetComponent<CStageManager>().GetGoalPoint();

        if (add_point > goal_point - curr_point) add_point = goal_point - curr_point;

        m_pNotConnectedRewardText.GetComponent<Text>().text = add_point.ToString();
        m_pStageManger.GetComponent<CStageManager>().AddPoint(add_point, false);

        m_bReadComplete = true;
    }

    // 어플리케이션 종료에 대한 처리
    void OnApplicationQuit()
    {
        PlayerInfo playerInfo = m_pPlayer.GetComponent<CPlayer>().GetPlayerInfo();

        WriteFile(playerInfo);
    }

    // 파일에 쓰기
	public void WriteFile(PlayerInfo playerInfo)
    {
        StreamWriter sw = new StreamWriter( Application.dataPath + "/Data/PlayerData" );

        sw.WriteLine(playerInfo.GetClearMission().ToString());
        sw.WriteLine(playerInfo.GetCurrentMission().ToString());
        sw.WriteLine(playerInfo.GetCurrentPoint().ToString());
        sw.WriteLine(playerInfo.GetDiamond().ToString());
        sw.WriteLine(playerInfo.GetGold().ToString());
        sw.WriteLine(ArrayToString(playerInfo.GetEmployee()));
        sw.WriteLine(ArrayToString(playerInfo.GetEquipment()));
        sw.WriteLine(playerInfo.GetLegacyBuild().ToString());
        sw.WriteLine(playerInfo.GetLastDateTime().ToString());
        sw.WriteLine(playerInfo.GetOption().ToString());

        sw.Flush();
        sw.Close();
	}

    // 파일로 부터 읽기
    public bool ReadFile()
    {
        if (CheckFile())
        {
            string input;

            StreamReader sr = File.OpenText(Application.dataPath + "/Data/PlayerData");

            input = sr.ReadLine();
            m_pPlayerInfo.SetClearMission(int.Parse(input));

            input = sr.ReadLine();
            m_pPlayerInfo.SetCurrentMission(int.Parse(input));

            input = sr.ReadLine();
            m_pPlayerInfo.SetCurrentPoint(ulong.Parse(input));

            input = sr.ReadLine();
            m_pPlayerInfo.SetDiamond(ulong.Parse(input));

            input = sr.ReadLine();
            m_pPlayerInfo.SetGold(ulong.Parse(input));

            input = sr.ReadLine();
            StringToItemInfoList(input, 0);

            input = sr.ReadLine();
            StringToItemInfoList(input, 1);

            // 레거시 빌드 받기
            input = sr.ReadLine();

            // 시간 받기
            input = sr.ReadLine();
            m_pPlayerInfo.SetLastDateTime(System.DateTime.Parse(input));

            // 옵션 받기


            sr.Close(); // 파일 읽기후 반드시 해준다.

            return true;
        }

        return false;
    }

    // 파일 확인
    bool CheckFile()
    {
        FileInfo finfo = new FileInfo(Application.dataPath + "/Data/PlayerData");

        return finfo.Exists;
    }

    string ArrayToString(ItemInfo[] items)
    {
        int array_index = 0;

        string str = "[";
        if (items != null)
        {
            foreach (ItemInfo item in items)
            {
                if (array_index > 0) str += ", ";

                str += "[";
                str += item.itemCode.ToString();
                str += ", ";
                str += item.itemLevel.ToString();
                str += "]";

                array_index++;
            }
        }
        str += "]";

        return str;
    }

    string ArrayToString(int[] items)
    {
        int array_index = 0;

        string str = "[";
        if (items != null)
        {
            foreach (int item in items)
            {
                if (array_index > 0) str += ", ";

                str += item.ToString();

                array_index++;
            }
        }
        str += "]";

        return str;
    }

    void StringToItemInfoList(string input, int arrType)
    {
        int index = 0;
        int phasing = 0;
        bool continuity = false;

        ItemInfo myItemInfo = new ItemInfo();

        while (index < input.Length)
        {
            if (input[index] >= '0' && input[index] <= '9')
            {
                if (phasing == 0)
                {
                    myItemInfo.itemCode *= 10;
                    myItemInfo.itemCode += input[index] - 48;
                }
                else
                {
                    myItemInfo.itemLevel *= 10;
                    myItemInfo.itemLevel = input[index] - 48;
                    if (arrType == 0)
                    {
                        m_pPlayerInfo.SetEmployee(myItemInfo.itemCode, myItemInfo.itemLevel);
                        myItemInfo.itemCode = 0;
                        myItemInfo.itemLevel = 0;
                    }
                    else if (arrType == 1)
                    {
                        m_pPlayerInfo.SetEquipment(myItemInfo.itemCode, myItemInfo.itemLevel);
                        myItemInfo.itemCode = 0;
                        myItemInfo.itemLevel = 0;
                    }
                }

                continuity = true;
            }
            else
            {
                if (continuity == true) phasing = (phasing + 1) % 2;
                continuity = false;
            }

            index++;
        }
    }

    public bool GetReadComplete()
    {
        return m_bReadComplete;
    }
}

[System.Serializable]
public struct ItemInfo
{
    public int itemCode;
    public int itemLevel;
}

public class PlayerInfo
{
    private int m_nClearMission;
    private int m_nCurrentMission;
    private ulong m_ulCurrentPoint;

    private ulong m_ulDiamond;
    private ulong m_ulGold;

    private ItemInfo[] m_tEmployee;
    private ItemInfo[] m_tEquipment;
    private int m_tLegacyBuild;

    private System.DateTime m_tLastDateTime;

    private int m_nOption;

    public PlayerInfo(int nClearMission, int nCurrentMission, ulong ulCurrentPoint, ulong ulDiamond, ulong ulGold, ItemInfo[] tEmployee, ItemInfo[] tEquipment, int tLegacyBuild, System.DateTime tLastDateTime, int nOption)
    {
        m_nClearMission = nClearMission;
        m_nCurrentMission = nCurrentMission;
        m_ulCurrentPoint = ulCurrentPoint;

        m_ulDiamond = ulDiamond;
        m_ulGold = ulGold;

        if (tEmployee == null)
        {
            m_tEmployee = new ItemInfo[ConstantsRW.OBJ_ARRAY_SIZE];

            for (int i = 0; i < ConstantsRW.OBJ_ARRAY_SIZE; i++)
            {
                m_tEmployee[i].itemCode = i;
                m_tEmployee[i].itemLevel = 0;
            }
        }
        else
        {
            m_tEmployee = tEmployee;
        }

        if (tEquipment == null)
        {
            m_tEquipment = new ItemInfo[ConstantsRW.OBJ_ARRAY_SIZE];

            for (int i = 0; i < ConstantsRW.OBJ_ARRAY_SIZE; i++)
            {
                m_tEquipment[i].itemCode = i;
                m_tEquipment[i].itemLevel = 0;
            }
        }
        else
        {
            m_tEquipment = tEquipment;
        }

        m_tLegacyBuild = tLegacyBuild;

        m_tLastDateTime = tLastDateTime;

        m_nOption = nOption;
    }

    public int GetClearMission()
    {
        return m_nClearMission;
    }

    public void SetClearMission(int nClearMission)
    {
        m_nClearMission = nClearMission;
    }

    public int GetCurrentMission()
    {
        return m_nCurrentMission;
    }

    public void SetCurrentMission(int nCurrentMission)
    {
        m_nCurrentMission = nCurrentMission;
    }

    public void SetCurrentPoint(ulong ulCurrentPoint)
    {
        m_ulCurrentPoint = ulCurrentPoint;
    }

    public ulong GetCurrentPoint()
    {
        return m_ulCurrentPoint;
    }

    public ulong GetDiamond()
    {
        return m_ulDiamond;
    }

    public void SetDiamond(ulong nDiamond)
    {
        m_ulDiamond = nDiamond;
    }

    public ulong GetGold()
    {
        return m_ulGold;
    }

    public void SetGold(ulong nGold)
    {
        m_ulGold  = nGold;
    }

    public ItemInfo[] GetEmployee()
    {
        return m_tEmployee;
    }

    public void SetEmployee(int code, int level)
    {
        m_tEmployee[code].itemCode = code;
        m_tEmployee[code].itemLevel = level;
    }

    public ItemInfo[] GetEquipment()
    {
        return m_tEquipment;
    }

    public void SetEquipment(int code, int level)
    {
        m_tEquipment[code].itemCode = code;
        m_tEquipment[code].itemLevel = level;
    }

    public int GetLegacyBuild()
    {
        return m_tLegacyBuild;
    }

    public void SetLegacyBuild(int tLegacyBuild)
    {
        m_tLegacyBuild = tLegacyBuild;
    }

    public System.DateTime GetLastDateTime()
    {
        return m_tLastDateTime;
    }

    public void SetLastDateTime(System.DateTime tLastDateTime)
    {
        m_tLastDateTime = tLastDateTime;
    }

    public int GetOption()
    {
        return m_nOption;
    }

    public void SetOption(int nOption)
    {
        m_nOption = nOption;
    }
}