﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CGUI_GagebarManager : MonoBehaviour
{
    public GameObject[] m_pBlocks = new GameObject[10];
    public GameObject m_pCoveringBlock;

    private bool m_bStart = false;
    private int m_nCurrentBlockIndex = 0;
    private float m_fCurrentPoint = 0.0f;
    private float m_fOneBlockPoint = 50.0f;

    private float m_fGageBeginX = -350.0f;          // 게이지 시작 위치
    private float m_fBlockWidth = 70.0f;            // 블록 너비
    private float m_fCoveringAlpha = 0.25f;         // 충전중인 블록 투명도
    
    void Start()
    {
        m_bStart = true;

        Color colr = m_pBlocks[m_nCurrentBlockIndex].GetComponent<Image>().color;
        colr.a = m_fCoveringAlpha;
        m_pBlocks[m_nCurrentBlockIndex].GetComponent<Image>().color = colr;
        m_pBlocks[m_nCurrentBlockIndex].SetActive(true);
    }

    void Update()
    {
        BuildBlock();
    }

    void BuildBlock()
    {
        if (!m_bStart) return;

        Vector3 scale = new Vector3((m_fCurrentPoint / m_fOneBlockPoint), 1.0f, 1.0f);
        m_pCoveringBlock.transform.localScale = scale;

        Vector3 position = new Vector3(m_fGageBeginX + (m_fBlockWidth * m_nCurrentBlockIndex) + ((m_fBlockWidth * 0.5f) * (m_fCurrentPoint / m_fOneBlockPoint)), 0.0f, 0.0f);
        m_pCoveringBlock.transform.localPosition = position;

        
    }

    public void AddPoint(float point)
    {
        Vector3 scale, position;
        Color colr;

        m_fCurrentPoint += point;

        scale = new Vector3((m_fCurrentPoint / m_fOneBlockPoint), 1.0f, 1.0f);
        m_pCoveringBlock.transform.localScale = scale;

        position = new Vector3(m_fGageBeginX + (m_fBlockWidth * m_nCurrentBlockIndex) + ((m_fBlockWidth * 0.5f) * (m_fCurrentPoint / m_fOneBlockPoint)), 0.0f, 0.0f);
        m_pCoveringBlock.transform.localPosition = position;

        if (m_fCurrentPoint >= m_fOneBlockPoint)
        {
            if (m_nCurrentBlockIndex >= 9)
            {
                m_bStart = false;

                colr = m_pBlocks[m_nCurrentBlockIndex].GetComponent<Image>().color;
                colr.a = 1.0f;
                m_pBlocks[m_nCurrentBlockIndex].GetComponent<Image>().color = colr;

                scale = new Vector3(0.0f, 1.0f, 1.0f);
                m_pCoveringBlock.transform.localScale = scale;

                return;
            }

            colr = m_pBlocks[m_nCurrentBlockIndex].GetComponent<Image>().color;
            colr.a = 1.0f;
            m_pBlocks[m_nCurrentBlockIndex].GetComponent<Image>().color = colr;
            m_pBlocks[m_nCurrentBlockIndex].SetActive(true);

            m_nCurrentBlockIndex += 1;

            m_pBlocks[m_nCurrentBlockIndex].SetActive(true);
            colr = m_pBlocks[m_nCurrentBlockIndex].GetComponent<Image>().color;
            colr.a = m_fCoveringAlpha;
            m_pBlocks[m_nCurrentBlockIndex].GetComponent<Image>().color = colr;

            m_fCurrentPoint = m_fCurrentPoint - m_fOneBlockPoint;
        }
    }
}
