﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CGUI_GroupOfButton : MonoBehaviour
{
    public GameObject[] m_pButtons = new GameObject[10];

    public void DisableButtons()
    {
        for (int i = 0; i < 10; i++)
        {
            if (m_pButtons[i]) m_pButtons[i].GetComponent<Button>().enabled = false;
        }
    }

    public void EnaableButtons()
    {
        for (int i = 0; i < 10; i++)
        {
            if (m_pButtons[i]) m_pButtons[i].GetComponent<Button>().enabled = true;
        }
    }
}
