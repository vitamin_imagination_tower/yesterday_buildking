﻿using UnityEngine;
using System;
using System.Collections;
using QuickPool;
using UnityEngine.UI;

public class CGUI_RisingText : MonoBehaviour
{
    private Vector3 m_vPosition;
    private Color m_vColor;
    private float m_fElapsedTime;

    void Start()
    {
        m_vColor = this.GetComponent<GUIText>().color;
    }

    void Update()
    {
        // 위치 조절
        m_vPosition += Vector3.up * 2 * Time.deltaTime;
        Vector3 viewPort = Camera.main.WorldToViewportPoint(m_vPosition);
        this.transform.position = viewPort;

        // 투명도 조절
        m_fElapsedTime += Time.deltaTime;
        m_vColor.a = 1.0f * ((2.0f - m_fElapsedTime) / 2.0f);
        this.GetComponent<GUIText>().color = m_vColor;

        // 소멸 처리
        if (m_fElapsedTime >= 2.0f)
        {
            m_vColor.a = 255;
            this.GetComponent<GUIText>().color = m_vColor;
            this.gameObject.Despawn();


            m_vPosition.x = 0.0f;
            m_vPosition.y = 0.0f;
            m_vPosition.z = 0.0f;

            m_fElapsedTime = 0.0f;
        }

    }

    public void InitText(ulong text)
    {
        string s = "+";
        int string_cnt = 3;

        ulong value = (ulong)(text / 10000000000000000);
        if (value > 0)
        {
            s += Convert.ToString(value);
            s += "경"; //경

            text -= value * 10000000000000000;
            string_cnt--;

            if(string_cnt <= 0)
            {
                s += "개"; // 원
                this.GetComponent<GUIText>().text = s;
                return;
            }
        }

        value = (ulong)(text / 1000000000000);
        if (value > 0)
        {
            s += Convert.ToString(value);
            s += "조"; // 조

            text -= value * 1000000000000;
            string_cnt--;

            if (string_cnt <= 0)
            {
                s += "개"; // 원
                this.GetComponent<GUIText>().text = s;
                return;
            }
        }

        value = (ulong)(text / 100000000);
        if (value > 0)
        {
            s += Convert.ToString(value);
            s += "억"; // 억

            text -= value * 100000000;
            string_cnt--;

            if (string_cnt <= 0)
            {
                s += "개"; // 원
                this.GetComponent<GUIText>().text = s;
                return;
            }
        }

        value = (ulong)(text / 10000);
        if (value > 0)
        {
            s += Convert.ToString(value);
            s += "만"; // 만

            text -= value * 10000;
            string_cnt--;

            if (string_cnt <= 0)
            {
                s += "개"; // 원
                this.GetComponent<GUIText>().text = s;
                return;
            }
        }


        s += Convert.ToString(text);
        s += "개"; // 원
        this.GetComponent<GUIText>().text = s;
        return;
    }

    public void InitPosition(Vector3 position)
    {
        m_vPosition = position;

    }
}