﻿using UnityEngine;
using System.Collections;
using QuickPool;


public class CGoldBlockManager : MonoBehaviour
{
    // 골드블럭 획득 시, 버프를 90% 확률로 준다.
    // 그냥 확인 누르면 기본 버프
    // 광고 시청 확인 누르면 광고 보고온 후, 슈퍼 버프 획득

    // 10% 확률로 큐브를 획득할 수 있다.
    // 큐브를 획득하면 큐브함으로 이동되었다는 말과 함께 종료.

    // 큐브함 가면 시간 가는중.
    // 큐브 종류 4가지    
    

    public float SpawnTime;
    public float LimitTime;

    public float Timer;

    int Probability;
    int endValue = 1;

    bool BuffBlockAppear;
    bool CubeBlockAppear;
    bool GetFail;

    public GameObject BuffBlock;
    public GameObject CubeBlock;



    Vector3 Target;
    public float MovePerSec;
    Vector3 Direction;

   
    

    void Start ()
    {
        BuffBlockAppear = false;
        CubeBlockAppear = false;
    }
	
    void InitBuffBlock()
    {
        BuffBlock.SetActive(false);
        BuffBlock.SetActive(true);
        BuffBlock.transform.position = new Vector3(1000, Random.Range(100, 800), 0);
        Target = new Vector3(-1500, Random.Range(100, 800), 0);
    }

    void InitCubeBlock()
    {
        CubeBlock.SetActive(false);
        CubeBlock.SetActive(true);
        CubeBlock.transform.position = new Vector3(1000, Random.Range(100, 800), 0);
        Target = new Vector3(-1500, Random.Range(100, 800), 0);
    }

    public void ProMaster()
    {
        if (Random.Range(0, endValue) == 0)
        {
            Probability = GetNumber();
            endValue += 1;
           // print("낮은 확률은 굴릴수록 확률이 더 낮아진다 : " + endValue);
            //print("낮은 확률 획득 : " + Probability);

            if (endValue >= 50) // 낮은 점수가 뜰 확률이 2%이하면
            {
            //    print("극악의 낮은 확률까지 올라간다면 초기화! : " + endValue);
                endValue = 1;
            }
        }
        else
        {
            Probability = GetNumber();
           // print("높은 확률 획득 : " + Probability);
        }
        if (Probability <= 50)
            GetBuff();

        else if (Probability >= 95)
            GetCube();

        else
            GetFail = true;
    }

    int GetNumber()
    {
        return Random.Range(0, 100);
    }

    void GetBuff()
    {
        BuffBlockAppear = true;
        InitBuffBlock();
        Timer = 0.0f;
    }

    void GetCube()
    {
        CubeBlockAppear = true;
        InitCubeBlock();
        Timer = 0.0f;
    }


    


    void Update ()
    {
        Timer += Time.deltaTime;

        if (Timer >= SpawnTime) // 10초 지나면 확률 테스트
        {            
            ProMaster();
          //  Debug.Log("주사위를 던집니다(1~100)  50이하로 뜨면 버프 블럭 생성 성공!  이번 주사위 : " + Probability);
        }        
        else if (Timer >= LimitTime)
        {
            if (GetFail)
                GetBuff();
            
        }

        if (BuffBlockAppear)
            BuffBlock_Move();

        else if(CubeBlockAppear)
            CubeBlock_Move();

        if (BuffBlock.transform.position.x <= -200)   // 블럭을 사용자가 누르지 못하면 초기화
        {
            BuffBlockAppear = false;
            BuffBlock.SetActive(false);
        }

        else if (CubeBlock.transform.position.x <= -200)   // 블럭을 사용자가 누르지 못하면 초기화
        {
            CubeBlockAppear = false;
            CubeBlock.SetActive(false);
        }
    }

    void BuffBlock_Move()
    {


        BuffBlock.transform.Translate(new Vector3(MovePerSec * Time.deltaTime, 0, 0 ));
    }

    void CubeBlock_Move()
    {
        Vector3 angle = new Vector3(Target.x - transform.position.x, Target.y - transform.position.y, 0.0f);
        Direction = Vector3.Normalize(angle);

        CubeBlock.transform.position += Direction * MovePerSec * Time.deltaTime;
    }
}

