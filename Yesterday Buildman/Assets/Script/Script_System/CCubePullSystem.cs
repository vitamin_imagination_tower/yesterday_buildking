﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class CCubePullSystem : MonoBehaviour
{
    public CCubeItem[] Cube;

    public int Probability;
    public static int LimitCube = 0;
    public int Prob = 0;
    public Text CubeType;
    int endValue = 1;
    public List<CCubeItem> CubeItems = new List<CCubeItem>();
    public List<CCubeItem> items = new List<CCubeItem>();
    public Transform targetTransform;
    public CCubeItemDisplay itemDisplayPrefab;
    public GameObject FullBlock;




    public void CubeChance() // 진정한 뽑기 시스템!!
    {
        if (Random.Range(0, endValue) == 0)
        {
            Probability = GetLowNumber();
            endValue += 1;
            print("낮은 확률은 굴릴수록 확률이 더 낮아진다 : " + endValue);
            print("낮은 확률 획득 : " + Probability);

            if (endValue >= 50) // 낮은 점수가 뜰 확률이 2%이하면
            {
                print("극악의 낮은 확률까지 올라간다면 초기화! : " + endValue);
                endValue = 1;
            }
        }
        else
        {
            Probability = GetHighNumber();
            print("높은 확률 획득 : " + Probability);
        }
        CubeCreation();
        print("큐브 제한 " + LimitCube);

    }

    int GetLowNumber()
    {
        return Random.Range(0, 50);
    }

    int GetHighNumber()
    {
        return Random.Range(50, 100);
    }


    void CubeCreation()
    {
        if (LimitCube <= 4)
        {
            LimitCube += 1;
            if (Probability <= 5) // 5%
            {
                CubeItems.Add(Cube[0]);
            }

            else if (6 <= Probability && Probability <= 25) // 20%
            {
                CubeItems.Add(Cube[1]);
            }

            else if (26 <= Probability && Probability <= 50) // 25%
            {
                CubeItems.Add(Cube[2]);
            }

            else if (51 <= Probability && Probability <= 100) // 50%
            {
                CubeItems.Add(Cube[3]);
            }

            ItemDisplay(CubeItems);
        }

        else {
            FullBlock.SetActive(true);
            FullCube();
        }


    }

    public void GetCube()
    {
        if (LimitCube <= 4)
        {
            LimitCube += 1;
            if (Probability <= 5) // 5%
            {
                CubeItems.Add(Cube[0]);
            }

            else if (6 <= Probability && Probability <= 25) // 20%
            {
                CubeItems.Add(Cube[1]);
            }

            else if (26 <= Probability && Probability <= 50) // 25%
            {
                CubeItems.Add(Cube[2]);
            }

            else if (51 <= Probability && Probability <= 100) // 50%
            {
                CubeItems.Add(Cube[3]);
            }

            ItemDisplay(CubeItems);
        }
        FullBlock.gameObject.SetActive(false);
    }

    void FullCube()
    {
        if (Probability <= 5)
            CubeType.text = "다이아상자";
        else if (6 <= Probability && Probability <= 25)
            CubeType.text = "골드상자";
        else if (26 <= Probability && Probability <= 50)
            CubeType.text = "철제상자";
        else if (51 <= Probability && Probability <= 100)
            CubeType.text = "나무상자";
    }

    public void BuyCube(GameObject Cube) // 큐브삭제
    {
        LimitCube -= 1;
        //print(LimitCube);
        Destroy(Cube.gameObject);
    }

    public void ThrowCube(GameObject Cube) // 큐브 버리기
    {
        LimitCube -= 1;
        //print(LimitCube);
        Destroy(Cube.gameObject);
    }


    public void ItemDisplay(List<CCubeItem> items)
    {
        foreach (var item in items)
        {
            var display = Instantiate(itemDisplayPrefab);
            display.transform.SetParent(targetTransform, false);
            display.item = item;
            display.Change();
        }
        items.Clear();
    }
    
}

