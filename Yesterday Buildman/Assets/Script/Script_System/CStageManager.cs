﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[System.Serializable]
public struct Stage_Handler
{
    public ulong        m_ulCurrentPoint;
    public ulong        m_ulGoalPoint;

    public bool         m_bPlaying;
}

[System.Serializable]
public struct Tower_Handler
{
    public GameObject[] m_pTowerList;
    public GameObject   m_pCurrentOrder;
}

public class CStageManager : MonoBehaviour
{
    public GameObject m_pPlayerManager;         // 플레이어 매니저 객체
    public Stage_Handler m_pStageHandler;       // 스테이지 핸들러
    public Tower_Handler m_pTowerHandler;       // 타워 핸들러

    public Text BuildName;

    public GameObject m_pEffect_MissionComplete;

    // 모래성 단계 0 ~ 4 
    // 돌     단계 5 ~ 9
    // 주택   단계  10 ~ 14
    public int LowLevel = 0;
    public int HighLevel = 4;
    public int NextRandom;

    int Temp_LowLevel;
    int Temp_Value;

    void Start()
    {
        m_pStageHandler.m_bPlaying = true;
    }

    void Update()
    {
        if (m_pStageHandler.m_bPlaying == false) NextOrder();
    }

    void LevelSelected(int nowBuilding)
    {
        // 현재까지 레벨
        switch (nowBuilding)
        {
            case 0:
                LowLevel = 0;
                HighLevel = 4;
                break;
            case 1:
                LowLevel = 5;
                HighLevel = 9;
                break;
            case 2:
                LowLevel = 10;
                HighLevel = 14;
                break;
        }
    }

    public void LoadStage(int nCurrentTower, ulong ulCurrentPoint)
    {
        switch (nCurrentTower)
        {
            case 0:
                BuildName.text = "아주 작은 모래성";
                break;
            case 1:
                BuildName.text = "작은 모래성";
                break;
            case 2:
                BuildName.text = "적당한 모래성";
                break;
            case 3:
                BuildName.text = "대형 모래성";
                break;
            case 4:
                BuildName.text = "초대형 모래성";
                break;
            case 5:
                BuildName.text = "고인돌";
                break;
            case 6:
                BuildName.text = "모아이";
                break;
            case 7:
                BuildName.text = "석등";
                break;
            case 8:
                BuildName.text = "석탑";
                break;
            case 9:
                BuildName.text = "신전";
                break;
            case 10:
                BuildName.text = "10평형 주택";
                break;
            case 11:
                BuildName.text = "18평형 주택";
                break;
            case 12:
                BuildName.text = "34평형 주택";
                break;
            case 13:
                BuildName.text = "60평형 주택";
                break;
            case 14:
                BuildName.text = "99평형 주택";
                break;
            case 15:
                BuildName.text = "경복궁";
                break;
        }
        m_pTowerHandler.m_pCurrentOrder.SetActive(false);
        m_pTowerHandler.m_pCurrentOrder = m_pTowerHandler.m_pTowerList[nCurrentTower];

        m_pStageHandler.m_ulCurrentPoint = ulCurrentPoint;
        m_pStageHandler.m_ulGoalPoint = m_pTowerHandler.m_pCurrentOrder.GetComponent<CBuildInfo>().GetPoint();
        m_pTowerHandler.m_pCurrentOrder.SetActive(true);

        m_pTowerHandler.m_pCurrentOrder.GetComponent<CBuildInfo>().ClipCurtain((float)m_pStageHandler.m_ulCurrentPoint / (float)m_pStageHandler.m_ulGoalPoint);
    }
    
    void TempData()
    {
        Temp_LowLevel = LowLevel;

        switch (Temp_LowLevel)
        {
            case 0:
                Temp_Value = 0;
                break;
            case 5:
                Temp_Value = 1;
                break;
            case 10:
                Temp_Value = 2;
                break;
        }
    }


    public void LegacyOrder(int OrderNumber)
    {
        // 보상을 저장합니다
        ulong reward = m_pTowerHandler.m_pCurrentOrder.GetComponent<CBuildInfo>().GetReward();

        switch (OrderNumber)
        {
            case 15:
                BuildName.text = "경복궁";
                break;
        }


        // 레거시 스테이지로 넘어갑니다
        m_pTowerHandler.m_pCurrentOrder.SetActive(false);
        m_pTowerHandler.m_pCurrentOrder = m_pTowerHandler.m_pTowerList[OrderNumber]; // Range 설정

        m_pStageHandler.m_ulCurrentPoint = 0;
        m_pStageHandler.m_ulGoalPoint = m_pTowerHandler.m_pCurrentOrder.GetComponent<CBuildInfo>().GetPoint();
        m_pTowerHandler.m_pCurrentOrder.GetComponent<CBuildInfo>().ClipCurtain((float)m_pStageHandler.m_ulCurrentPoint / (float)m_pStageHandler.m_ulGoalPoint);

        m_pTowerHandler.m_pCurrentOrder.SetActive(true);


        m_pStageHandler.m_bPlaying = true;
        Instantiate(m_pEffect_MissionComplete, new Vector3(-7, 21, -35), Quaternion.identity);
        TempData();
        LevelSelected(Temp_Value);

    }

    public void NextOrder()
    {
        // 보상을 저장합니다
        ulong reward = m_pTowerHandler.m_pCurrentOrder.GetComponent<CBuildInfo>().GetReward();

        // 여기 조건문으로 LowLevel을 높히는 조건문이 있으면된다.

        NextRandom = Random.Range(LowLevel, HighLevel);

        switch(NextRandom)
        {
            case 0:
                BuildName.text = "아주 작은 모래성";
                break;
            case 1:
                BuildName.text = "작은 모래성";
                break;
            case 2:
                BuildName.text = "적당한 모래성";
                break;
            case 3:
                BuildName.text = "대형 모래성";
                break;
            case 4:
                BuildName.text = "초대형 모래성";
                break;
            case 5:
                BuildName.text = "고인돌";
                break;
            case 6:
                BuildName.text = "모아이";
                break;
            case 7:
                BuildName.text = "석등";
                break;
            case 8:
                BuildName.text = "석탑";
                break;
            case 9:
                BuildName.text = "신전";
                break;
            case 10:
                BuildName.text = "10평형 주택";
                break;
            case 11:
                BuildName.text = "18평형 주택";
                break;
            case 12:
                BuildName.text = "34평형 주택";
                break;
            case 13:
                BuildName.text = "60평형 주택";
                break;
            case 14:
                BuildName.text = "99평형 주택";
                break;
            case 15:
                BuildName.text = "경복궁";
                break;
        }
        
        // 다음 스테이지로 넘어갑니다
        m_pTowerHandler.m_pCurrentOrder.SetActive(false);
        m_pTowerHandler.m_pCurrentOrder = m_pTowerHandler.m_pTowerList[NextRandom]; // Range 설정

        m_pStageHandler.m_ulCurrentPoint = 0;
        m_pStageHandler.m_ulGoalPoint = m_pTowerHandler.m_pCurrentOrder.GetComponent<CBuildInfo>().GetPoint();
        m_pTowerHandler.m_pCurrentOrder.GetComponent<CBuildInfo>().ClipCurtain((float)m_pStageHandler.m_ulCurrentPoint / (float)m_pStageHandler.m_ulGoalPoint);

        m_pTowerHandler.m_pCurrentOrder.SetActive(true);

        // 이번 스테이지 보상을 받습니다
        m_pPlayerManager.GetComponent<CPlayer>().AddGold(reward);

        m_pStageHandler.m_bPlaying = true;
        Instantiate(m_pEffect_MissionComplete, new Vector3(-7, 21, -35), Quaternion.identity);
    }

    public void AddPoint(ulong point, bool bCheckPlayState = true)
    {
        if (m_pStageHandler.m_bPlaying == false && bCheckPlayState == true) return;

        m_pStageHandler.m_ulCurrentPoint += point;

        if (m_pStageHandler.m_ulCurrentPoint >= m_pStageHandler.m_ulGoalPoint)
        {
            m_pStageHandler.m_ulCurrentPoint = m_pStageHandler.m_ulGoalPoint;
            m_pStageHandler.m_bPlaying = false;
        }

        m_pTowerHandler.m_pCurrentOrder.GetComponent<CBuildInfo>().ClipCurtain((float)m_pStageHandler.m_ulCurrentPoint / (float)m_pStageHandler.m_ulGoalPoint);
    }

    public int GetCurrentStage()
    {
        return m_pTowerHandler.m_pCurrentOrder.GetComponent<CBuildInfo>().GetStage();
    }

    public ulong GetCurrentPoint()
    {
        return m_pStageHandler.m_ulCurrentPoint;
    }

    public ulong GetGoalPoint()
    {
        return m_pStageHandler.m_ulGoalPoint;
    }
}