﻿using UnityEngine;
using System.Collections;

public class CBuildInfo : MonoBehaviour
{
    public int m_nStage;
    public ulong m_ulPoint;
    public ulong m_ulReward;

    public float m_fBeginClipY;
    public float m_fClipLength;

    public GameObject m_pCurtain;
    private Material m_pCurtainMaterial;
    private float m_fScreenHeight;
    private bool m_bClipAble = false;


    void Start()
    {
        m_pCurtainMaterial = m_pCurtain.GetComponent<SpriteRenderer>().material;

        m_fScreenHeight = Camera.main.pixelHeight;
        m_pCurtainMaterial.SetInt("_Height", (int)m_fScreenHeight);
        m_bClipAble = true;
    }

    public void ClipCurtain(float fClip)
    {
        if (m_bClipAble)
        {
            m_pCurtainMaterial.SetInt("_ClipY", (int)(-(m_fBeginClipY * m_fScreenHeight) + (m_fScreenHeight * m_fClipLength * fClip)));
        }
        else
        {
            m_pCurtainMaterial = m_pCurtain.GetComponent<SpriteRenderer>().material;

            m_fScreenHeight = Camera.main.pixelHeight;
            m_pCurtainMaterial.SetInt("_Height", (int)m_fScreenHeight);
            m_bClipAble = true;
        }
    }

    public int GetStage()
    {
        return m_nStage;
    }

    public ulong GetPoint()
    {
        return m_ulPoint;
    }

    public ulong GetReward()
    {
        return m_ulReward;
    }
}
