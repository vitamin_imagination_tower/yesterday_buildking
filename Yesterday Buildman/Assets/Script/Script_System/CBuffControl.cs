﻿using UnityEngine;
using System.Collections;

public class CBuffControl : MonoBehaviour {

    public CClickBuff ClBuff;
    public CBPCBuff BPCBuff;
    public CBPSBuff BPSBuff;

    // UI
    public GameObject Click;
    public GameObject BPC;
    public GameObject BPS;
    int Prob;
    float ExistTime;


    public bool BuffExist;
    
    public void BuffPull()
    {
        Prob = Random.Range(1, 100);

        if (!BuffExist)
        {
            if (Prob < 34)
            {
                Click.SetActive(true);
                ClBuff.BuffState();
                BuffExist = true;
            }

            else if (Prob > 33 && Prob < 67)
            {
                BPC.SetActive(true);
                BPCBuff.BuffState();
                BuffExist = true;
            }
        }

        if (Prob > 66)
        {
            BPS.SetActive(true);
            BPSBuff.BuffState();
        }
    }


    void Update()
    {
        if (BuffExist)
        {
            ExistTime += Time.deltaTime;
            if (ExistTime >= 10)
            {
                ExistTime = 0;
                BuffExist = false;
            }
        }

        else
            ExistTime = 0;


    }
}
