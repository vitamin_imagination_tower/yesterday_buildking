﻿using UnityEngine;
using UnityEngine.UI;

public class CCubeItemDisplay : MonoBehaviour {

    public Text Name;         // 상자 이름
    public Image Cube;        // 상자 이미지
    public Text CountTime;    // 보상까지 남은 시간
    public Text Buy;          // 즉시 구매
    public Text Throw;        // 버리기    
    public CCubeItem item;

    public delegate void CCubeItemDisplayDelegate(CCubeItem item);

    float thisTimer;
    public float HourTime; // 시
    public float MinTime; // 분
    public float SecTime; // 초

    void Start()
    {
        thisTimer = item.ItemTimer;
        HourTime = thisTimer / 3600;
        MinTime = (thisTimer % 3600) / 60;
        SecTime = ((thisTimer % 3600) % 60);
    }

    void Update()
    {
        thisTimer -= Time.deltaTime;

        HourTime = (int)(thisTimer / 3600);
        MinTime = (int)(thisTimer % 3600 / 60);
        SecTime = (int)((thisTimer % 3600) % 60);
        CountTime.text = HourTime + ":" + MinTime + ":" + SecTime;
    }

    public void AdsPower()
    {
        thisTimer *= 0.5f;
    }

    public void Change()
    {
        if (Name != null)
            Name.text = item.ItemName;
        if (Cube != null)
            Cube.sprite = item.ItemImage;
        if (CountTime != null)
            CountTime.text = "" + HourTime + ":" + MinTime + ":" + SecTime;
        if (Buy != null)
            Buy.text = "즉시 구매 : " + item.ItemTimer / 300;
        if (Throw != null)
            Throw.text = "버리기 : " + 5;
    }
}
