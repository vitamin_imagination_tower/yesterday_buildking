﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

static class CONST_OF_DATA_LOAD
{
    public const float MIN_LOAD_TIME = 2.0f;
}

public class CSceneLoader : MonoBehaviour
{
    public GameObject m_pReadWriteHandler;
    public GameObject m_pMainSceneHandler;

    private bool    m_bReadDataComplete;
    private bool    m_bChangeScene;
    private float   m_fElapsedTime;

    void Start()
    {
        m_bChangeScene = false;
        m_fElapsedTime = 0.0f;
    }

    void Update()
    {
        if (m_fElapsedTime < CONST_OF_DATA_LOAD.MIN_LOAD_TIME)
        {
            m_fElapsedTime += Time.deltaTime;

            if (m_fElapsedTime >= CONST_OF_DATA_LOAD.MIN_LOAD_TIME &&
                m_pReadWriteHandler.GetComponent<ReadWriteData>().GetReadComplete() == true)
            {
                m_bChangeScene = true;

                transform.FindChild("sprite_loading_0").gameObject.SetActive(false);
                transform.FindChild("sprite_loading_1").gameObject.SetActive(true);
            }
            return;
        }

        if ((Input.GetMouseButtonDown(0)) && (m_bChangeScene == true))
        {
            this.ChangeScene();
        }
    }

    // 메인 씬으로 변경합니다
    //
    public void ChangeScene()
    {
        m_pMainSceneHandler.gameObject.SetActive(true);
        this.gameObject.SetActive(false);
    }
}
