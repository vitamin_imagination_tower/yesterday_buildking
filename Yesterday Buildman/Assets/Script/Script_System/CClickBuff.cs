﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CClickBuff : MonoBehaviour
{
    float BuffTime;

    public Text ClickBuffText;
    public CPlayer player;
    bool BuffExist;

    ulong BuffValue;
    int BuffProb;
    ulong BPC;

    public void BuffState()
    {
        if (!BuffExist)
        {
            BPC = player.m_ulBPC;
            BuffProb = Random.Range(0, 100);
            if (BuffTime <= 10)
            {
                if (BuffProb <= 10)
                {
                    ClickBuffText.text = "대 버프 / 10초간 BPC 2.0배";
                    BuffValue = 200;
                    player.m_ulBPC = player.m_ulBPC * BuffValue / 100;
                }
                else if (BuffProb >= 11 && BuffProb <= 40)
                {
                    ClickBuffText.text = "중 버프 / 10초간 BPC 1.5배";
                    BuffValue = 150;
                    player.m_ulBPC = player.m_ulBPC * BuffValue / 100;
                }
                else if (BuffProb >= 41 && BuffProb <= 100)
                {
                    ClickBuffText.text = "소 버프 / 10초간 BPC 1.2배";
                    BuffValue = 120;
                    player.m_ulBPC = player.m_ulBPC * BuffValue / 100;
                }
            }
        }
    }

    void Update()
    {
        
        BuffTime += Time.deltaTime;
        if (BuffTime >= 10)
        {
            BuffExist = false;
            BuffTime = 0;
            BuffValue = 1;
            player.m_ulBPC = BPC;
            ClickBuffText.text = "";
            this.gameObject.SetActive(false);
        }
     
    }
}
