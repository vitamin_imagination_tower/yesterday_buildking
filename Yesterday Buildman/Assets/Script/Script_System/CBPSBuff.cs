﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CBPSBuff : MonoBehaviour
{
    float BuffTime;

    public Text BPSBuffText;

    public CPlayer player;

    int BuffProb;
    ulong BuffValue;
    ulong BPS;

    bool BuffExist;

    public void BuffState()
    {
        if (!BuffExist)
        {
            BPS = player.m_ulBPS;
            BuffProb = Random.Range(0, 100);
            if (BuffTime <= 10)
            {
                if (BuffProb <= 10)
                {
                    BPSBuffText.text = "대 버프 / 10초간 BPS 30%증가";
                    BuffValue = 130;
                    player.m_ulBPS = player.m_ulBPS * BuffValue / 100;
                    BuffExist = true;
                }
                else if (BuffProb >= 11 && BuffProb <= 40)
                {
                    BPSBuffText.text = "중 버프 / 10초간 BPS 20%증가";
                    BuffValue = 120;
                    player.m_ulBPS = player.m_ulBPS * BuffValue / 100;
                    BuffExist = true;
                }
                else if (BuffProb >= 41 && BuffProb <= 100)
                {
                    BPSBuffText.text = "소 버프 / 10초간 BPS 10%증가";
                    BuffValue = 110;
                    player.m_ulBPS = player.m_ulBPS * BuffValue / 100;
                    BuffExist = true;
                }
            }
        }
    }

    void Update()
    {

        BuffTime += Time.deltaTime;
        if (BuffTime >= 10)
        {
            BuffExist = false; 
            BuffTime = 0;
            BuffValue = 1;
            player.m_ulBPS = BPS;
            BPSBuffText.text = "";
            this.gameObject.SetActive(false);
        }

    }
}
