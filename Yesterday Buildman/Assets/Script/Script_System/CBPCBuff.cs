﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CBPCBuff : MonoBehaviour
{
    float BuffTime;

    public Text BPCBuffText;
    public CPlayer player;

    int BuffProb;
    float BuffValue;
    float m_fElapseTime;
    float m_fEndTime;

    bool BuffExist;


    public void BuffState()
    {
        if (!BuffExist)
        {
            BuffProb = Random.Range(0, 100);
            if (BuffTime <= 10)
            {
                if (BuffProb <= 10)
                {
                    BPCBuffText.text = "대 버프 / 10초간 60번 자동 클릭";
                    BuffValue = 60;
                    m_fEndTime = 0.125f;
                    BuffExist = true;
                }
                else if (BuffProb >= 11 && BuffProb <= 40)
                {
                    BPCBuffText.text = "중 버프 / 10초간 40번 자동 클릭";
                    BuffValue = 40;
                    m_fEndTime = 0.25f;
                    BuffExist = true;
                }
                else if (BuffProb >= 41 && BuffProb <= 100)
                {
                    BPCBuffText.text = "소 버프 / 10초간 20번 자동 클릭";
                    BuffValue = 20;
                    m_fEndTime = 0.5f;
                    BuffExist = true;
                }
            }
        }
    }

    void Update()
    {
        m_fElapseTime += Time.deltaTime;

        for (int i = 0; i < BuffValue; ++i)
        { 
            if (m_fElapseTime >= m_fEndTime)
            {
                player.BuffTouch();
                m_fElapseTime = 0.0f;
            }
        }

        BuffTime += Time.deltaTime;
        if (BuffTime >= 10)
        {
            BuffExist = false;
            BuffValue = 0;
            BuffTime = 0;
            BPCBuffText.text = "";
            this.gameObject.SetActive(false);
        }

    }
}
