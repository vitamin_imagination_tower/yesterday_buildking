﻿using UnityEngine;
using System.Collections;
using QuickPool;
using UnityEngine.UI; 

public class CPlayer : MonoBehaviour
{
    public GameObject m_pStageManager;
    public GameObject m_pBPSText;
    public GameObject m_pBPCText;

    private float m_fElapseTime;
    private int m_nMaxTouch = 50;
    private int m_nTouch;

    public GameObject m_pDiamondBar;
    public GameObject m_pGoldBar;
    public GameObject m_pBPCBar;
    public GameObject m_pBPSBar;

    private ulong m_ulDiamond = 0;
    private ulong m_ulGold = 0;
    private int m_nCurrentMission = 0;
    public ulong m_ulBPC;
    public ulong m_ulBPS;

    private int m_nTotalClearMission = 0;
    private int m_nOption = 0;


    private ItemInfo[] m_tEmployee;
    private ItemInfo[] m_tEquipment;
    private int m_nLegacyBuild;

    private System.DateTime m_tLastDateTime;

	void Start ()
    {
        this.RefreshDiamondBar();
        this.RefreshGoldBar();
        this.RefreshBPCBar();
        this.RefreshBPSBar();
	}
	
	void Update ()
    {
        m_fElapseTime += Time.deltaTime;

        if(m_fElapseTime >= 1.0f)
        {
            m_fElapseTime -= 1.0f;
            m_nTouch = 0;

            GameObject risingText = m_pBPSText.Spawn(this.transform.position, Quaternion.identity) as GameObject;
            risingText.GetComponent<CGUI_RisingText>().InitText(m_ulBPS);

            m_pStageManager.GetComponent<CStageManager>().AddPoint(m_ulBPS);
        }
	}

    public void Touch()
    {
        if (m_nTouch < m_nMaxTouch)
        {
            m_nTouch += 1;

            GameObject risingText = m_pBPCText.Spawn(this.transform.position, Quaternion.identity) as GameObject;
            risingText.GetComponent<CGUI_RisingText>().InitText(m_ulBPC);

            Vector3 curPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.WorldToScreenPoint(risingText.transform.position).z);
            Vector3 worldPos = Camera.main.ScreenToWorldPoint(curPos);

            risingText.GetComponent<CGUI_RisingText>().InitPosition(worldPos);

            m_pStageManager.GetComponent<CStageManager>().AddPoint(m_ulBPC);
        }
    }

    public void BuffTouch()
    {
        GameObject risingText = m_pBPCText.Spawn(this.transform.position, Quaternion.identity) as GameObject;
        risingText.GetComponent<CGUI_RisingText>().InitText(m_ulBPC);

        Vector3 curPos = new Vector3(533, 261, 77);
        Vector3 worldPos = Camera.main.ScreenToWorldPoint(curPos);

        risingText.GetComponent<CGUI_RisingText>().InitPosition(worldPos);

        m_pStageManager.GetComponent<CStageManager>().AddPoint(m_ulBPC);
    }

    public void RefreshDiamondBar()
    {
        m_pDiamondBar.GetComponent<Text>().text = m_ulDiamond.ToString();
    }

    public void RefreshGoldBar()
    {
        m_pGoldBar.GetComponent<Text>().text = m_ulGold.ToString();
    }

    public void RefreshBPCBar()
    {
        m_pBPCBar.GetComponent<Text>().text = m_ulBPC.ToString("#,#0.0");
    }

    public void RefreshBPSBar()
    {
        m_pBPSBar.GetComponent<Text>().text = m_ulBPS.ToString("#,#0.0");
    }

    public ulong GetBPC()
    {
        return m_ulBPC;
    }

    public ulong GetBPS()
    {
        return m_ulBPS;
    }

    public void SetGold(ulong ulGold)
    {
        m_ulGold = ulGold;

        this.RefreshGoldBar();
    }

    public void SetDiamond(ulong ulDiamond)
    {
        m_ulDiamond = ulDiamond;

        this.RefreshDiamondBar();
    }

    public ulong GetGold()
    {
        return m_ulGold;
    }

    public void AddGold(ulong ulGold)
    {
        m_ulGold += ulGold;

        this.RefreshGoldBar();
    }

    public void DelGold(ulong ulGold)
    {
        m_ulGold -= ulGold;

        this.RefreshGoldBar();
    }

    public ulong GetDiamond()
    {
        return m_ulDiamond;
    }

    public void AddDiamond(ulong ulDiamond)
    {
        m_ulDiamond += ulDiamond;

        this.RefreshDiamondBar();
    }

    public void DelDiamond(ulong ulDiamond)
    {
        m_ulGold -= ulDiamond;

        this.RefreshDiamondBar();
    }

    public System.DateTime GetLastDateTime()
    {
        return m_tLastDateTime;
    }

    public PlayerInfo GetPlayerInfo()
    {
        PlayerInfo playerInfo = new PlayerInfo(
            0,
            m_pStageManager.GetComponent<CStageManager>().GetCurrentStage(), 
            m_pStageManager.GetComponent<CStageManager>().GetCurrentPoint(),
            m_ulDiamond, 
            m_ulGold,
            m_tEmployee, 
            m_tEquipment,
            m_nLegacyBuild,
            System.DateTime.Now,
            0);

        return playerInfo;
    }

    public void SetPlayerInfo(PlayerInfo playerinfo)
    {
        m_ulDiamond = playerinfo.GetDiamond();
        m_ulGold = playerinfo.GetGold();
        m_nTotalClearMission = playerinfo.GetClearMission();
        m_tEmployee = playerinfo.GetEmployee();
        m_tEquipment = playerinfo.GetEquipment();
        m_nLegacyBuild = playerinfo.GetLegacyBuild();
        m_tLastDateTime = playerinfo.GetLastDateTime();
    }

    public void SetEmployeeLevel(int key, int level)
    {
        m_tEmployee[key].itemLevel = level;
    }

    public int GetEmployeeLevel(int key)
    {
        return m_tEmployee[key].itemLevel;
    }

    public void SetEquipmentLevel(int key, int level)
    {
        m_tEquipment[key].itemLevel = level;
    }

    public int GetEquipmentLevel(int key)
    {
        return m_tEquipment[key].itemLevel;
    }
}
