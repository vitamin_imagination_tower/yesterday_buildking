﻿using UnityEngine;
using System.Collections;

public class CCrowdAI : MonoBehaviour {

    public GameObject Skel_Child_0;
    public GameObject Skel_Child_1;
    public GameObject Skel_Child_2;
    public GameObject Skel_Child_3;
    public GameObject Skel_Child_4;
    public GameObject Skel_Child_5;
    public GameObject Skel_Child_6;
    public GameObject Skel_Child_7;
    public GameObject Skel_Child_8;

    public WorkAI_States m_tWorkStates;
    protected Vector3 m_vDirection;
    protected int m_nCurrentIntersection;
    protected float m_fAnimateTime;
    bool bEquipmentExist = false;

    public int m_empType;

    void Start()
    {
        SetAnimation(m_tWorkStates.m_nAction);
        SetDestination(true);
    }

    void Update()
    {
        int index;
        Vector3 forward;

        for (index = 0; index < m_tWorkStates.m_vIntersection.Length - 1; index++)
        {
            forward = (m_tWorkStates.m_vIntersection[index + 1] - m_tWorkStates.m_vIntersection[index]);
            Debug.DrawRay(m_tWorkStates.m_vIntersection[index] + (Vector3.up * 0.5f), forward, m_tWorkStates.m_vColor);
        }

        forward = (m_tWorkStates.m_vIntersection[0] - m_tWorkStates.m_vIntersection[index]);
        Debug.DrawRay(m_tWorkStates.m_vIntersection[index] + (Vector3.up * 0.5f), forward, m_tWorkStates.m_vColor);

        switch (m_tWorkStates.m_nAction)
        {
            case WorkAI_ACTION.IDLE:
                m_fAnimateTime += Time.deltaTime;
                if (m_fAnimateTime >= m_tWorkStates.m_fMinIdleTime)
                {
                    // 이동 상태로 변경
                    SetAnimation(WorkAI_ACTION.WALK);
                }
                break;

            case WorkAI_ACTION.WALK:
                this.transform.position += m_vDirection * m_tWorkStates.m_fMovePerSec;
                if (Vector3.Distance(this.transform.position, m_tWorkStates.m_vIntersection[m_nCurrentIntersection]) < 0.1f)
                {
                    SetDestination(false);
                }

                m_fAnimateTime += Time.deltaTime;
                if (m_fAnimateTime >= 1.0f)
                {
                    m_fAnimateTime = 0.0f;
                }
                break;

            case WorkAI_ACTION.WALK_WORK:
                this.transform.position += m_vDirection * m_tWorkStates.m_fMovePerSec;
                if (Vector3.Distance(this.transform.position, m_tWorkStates.m_vIntersection[m_nCurrentIntersection]) < 0.1f)
                {
                    SetDestination(false);
                }
                break;

            case WorkAI_ACTION.GET_WORK:
                m_fAnimateTime += Time.deltaTime;
                if (m_fAnimateTime >= m_tWorkStates.m_fMinWorkTime)
                {
                    SetDestination(true);
                    GetEquipment();

                    // 이동 상태로 변경
                    if (m_empType == 1)
                        SetAnimation(WorkAI_ACTION.WALK);
                    else if (m_empType == 2)
                        SetAnimation(WorkAI_ACTION.WALK_WORK);
                }
                break;
            case WorkAI_ACTION.SET_WORK:
                m_fAnimateTime += Time.deltaTime;
                if (m_fAnimateTime >= m_tWorkStates.m_fMinWorkTime)
                {
                    SetDestination(true);
                    SetEquipment();
                    // 이동 상태로 변경
                    SetAnimation(WorkAI_ACTION.WALK);
                }
                break;
        }
    }

    void GetEquipment()
    {
        m_tWorkStates.m_pGetEquipment.SetActive(true);
        m_tWorkStates.m_pSetEquipment.SetActive(false);
    }

    void SetEquipment()
    {
        m_tWorkStates.m_pGetEquipment.SetActive(false);
        m_tWorkStates.m_pSetEquipment.SetActive(true);
    }

    void SetDestination(bool bWorkCheckSkip)
    {
        Quaternion quaternion;

        // 현재 위치를 도착 지점에 보간
        this.transform.position = m_tWorkStates.m_vIntersection[m_nCurrentIntersection];

        // 제 1의 도착 지점이 작업 지점이라면 작업을 수행
        if (!bWorkCheckSkip && !bEquipmentExist && m_nCurrentIntersection == m_tWorkStates.m_nGetSection)
        {
            m_vDirection = (m_tWorkStates.m_pGetSection.transform.position - this.transform.position).normalized;
            quaternion = Quaternion.LookRotation(m_vDirection);
            transform.rotation = quaternion;

            SetAnimation(WorkAI_ACTION.GET_WORK);

            bEquipmentExist = true;
            return;
        }

        // 제 2의 도착 지점이 작업 지점이라면 작업을 수행
        if (!bWorkCheckSkip && bEquipmentExist && m_nCurrentIntersection == m_tWorkStates.m_nSetSection)
        {
            m_vDirection = (m_tWorkStates.m_pSetSection.transform.position - this.transform.position).normalized;
            quaternion = Quaternion.LookRotation(m_vDirection);
            transform.rotation = quaternion;

            SetAnimation(WorkAI_ACTION.SET_WORK);

            bEquipmentExist = false;
            return;
        }


        // 다음 목표 지점을 지정
        m_nCurrentIntersection++;

        if (m_nCurrentIntersection >= m_tWorkStates.m_vIntersection.Length) m_nCurrentIntersection = 0;

        // 목표을 향하도록 회전
        m_vDirection = (m_tWorkStates.m_vIntersection[m_nCurrentIntersection] - this.transform.position).normalized;
        quaternion = Quaternion.LookRotation(m_vDirection);
        transform.rotation = quaternion;
    }



    void SetAnimation(WorkAI_ACTION action)
    {
        m_tWorkStates.m_nAction = action;
        m_fAnimateTime = 0.0f;

        switch (m_tWorkStates.m_nAction)
        {
            case WorkAI_ACTION.IDLE:
                GetComponent<Animation>().Play("IDLE");
                Skel_Child_0.GetComponent<Animation>().Play("IDLE");
                Skel_Child_1.GetComponent<Animation>().Play("IDLE");
                Skel_Child_2.GetComponent<Animation>().Play("IDLE");
                Skel_Child_3.GetComponent<Animation>().Play("IDLE");
                Skel_Child_4.GetComponent<Animation>().Play("IDLE");
                Skel_Child_5.GetComponent<Animation>().Play("IDLE");
                Skel_Child_6.GetComponent<Animation>().Play("IDLE");
                Skel_Child_7.GetComponent<Animation>().Play("IDLE");
                Skel_Child_8.GetComponent<Animation>().Play("IDLE");
                break;

            case WorkAI_ACTION.WALK:
                GetComponent<Animation>().Play("WALK");
                Skel_Child_0.GetComponent<Animation>().Play("WALK");
                Skel_Child_1.GetComponent<Animation>().Play("WALK");
                Skel_Child_2.GetComponent<Animation>().Play("WALK");
                Skel_Child_3.GetComponent<Animation>().Play("WALK");
                Skel_Child_4.GetComponent<Animation>().Play("WALK");
                Skel_Child_5.GetComponent<Animation>().Play("WALK");
                Skel_Child_6.GetComponent<Animation>().Play("WALK");
                Skel_Child_7.GetComponent<Animation>().Play("WALK");
                Skel_Child_8.GetComponent<Animation>().Play("WALK");
                break;

            case WorkAI_ACTION.WALK_WORK:
                GetComponent<Animation>().Play("WALK_WORK");
                Skel_Child_0.GetComponent<Animation>().Play("WALK_WORK");
                Skel_Child_1.GetComponent<Animation>().Play("WALK_WORK");
                Skel_Child_2.GetComponent<Animation>().Play("WALK_WORK");
                Skel_Child_3.GetComponent<Animation>().Play("WALK_WORK");
                Skel_Child_4.GetComponent<Animation>().Play("WALK_WORK");
                Skel_Child_5.GetComponent<Animation>().Play("WALK_WORK");
                Skel_Child_6.GetComponent<Animation>().Play("WALK_WORK");
                Skel_Child_7.GetComponent<Animation>().Play("WALK_WORK");
                Skel_Child_8.GetComponent<Animation>().Play("WALK_WORK");
                break;

            case WorkAI_ACTION.GET_WORK:
                GetComponent<Animation>().Play("GET_WORK");
                Skel_Child_0.GetComponent<Animation>().Play("GET_WORK");
                Skel_Child_1.GetComponent<Animation>().Play("GET_WORK");
                Skel_Child_2.GetComponent<Animation>().Play("GET_WORK");
                Skel_Child_3.GetComponent<Animation>().Play("GET_WORK");
                Skel_Child_4.GetComponent<Animation>().Play("GET_WORK");
                Skel_Child_5.GetComponent<Animation>().Play("GET_WORK");
                Skel_Child_6.GetComponent<Animation>().Play("GET_WORK");
                Skel_Child_7.GetComponent<Animation>().Play("GET_WORK");
                Skel_Child_8.GetComponent<Animation>().Play("GET_WORK");
                break;

            case WorkAI_ACTION.SET_WORK:
                GetComponent<Animation>().Play("SET_WORK");
                Skel_Child_0.GetComponent<Animation>().Play("SET_WORK");
                Skel_Child_1.GetComponent<Animation>().Play("SET_WORK");
                Skel_Child_2.GetComponent<Animation>().Play("SET_WORK");
                Skel_Child_3.GetComponent<Animation>().Play("SET_WORK");
                Skel_Child_4.GetComponent<Animation>().Play("SET_WORK");
                Skel_Child_5.GetComponent<Animation>().Play("SET_WORK");
                Skel_Child_6.GetComponent<Animation>().Play("SET_WORK");
                Skel_Child_7.GetComponent<Animation>().Play("SET_WORK");
                Skel_Child_8.GetComponent<Animation>().Play("SET_WORK");
                break;


        }
    }
}
