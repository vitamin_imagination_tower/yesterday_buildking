﻿using UnityEngine;
using System.Collections;

public enum AI_ACTION
{
    IDLE,
    WALK,
    WORK1,
    WORK2
}

[System.Serializable]
public struct AI_States
{
    public AI_ACTION m_nAction;

    public Vector3[] m_vIntersection;
    public GameObject m_pWorkSection;
    public int m_nWorkSection;

    public Color m_vColor;
    public float m_fMinIdleTime;
    public float m_fMinWorkTime;
    public float m_fMovePerSec;
    public float m_fChangeIdlePerSec;
}

public class CAI : MonoBehaviour
{
    public AI_States    m_tStates;
    protected Vector3   m_vDirection;
    protected int       m_nCurrentIntersection;
    protected float     m_fAnimateTime;

    void Start()
    {
        SetAnimation(m_tStates.m_nAction);
        SetDestination(true);
    }

    void Update ()
    {
        int index;
        Vector3 forward;

        for (index = 0; index < m_tStates.m_vIntersection.Length - 1; index++)
        {
            forward = (m_tStates.m_vIntersection[index + 1] - m_tStates.m_vIntersection[index]);
            Debug.DrawRay(m_tStates.m_vIntersection[index] + (Vector3.up * 0.5f), forward, m_tStates.m_vColor);
        }

        forward = (m_tStates.m_vIntersection[0] - m_tStates.m_vIntersection[index]);
        Debug.DrawRay(m_tStates.m_vIntersection[index] + (Vector3.up * 0.5f), forward, m_tStates.m_vColor);

	    switch(m_tStates.m_nAction)
        {
            case AI_ACTION.IDLE:
                m_fAnimateTime += Time.deltaTime;
                if (m_fAnimateTime >= m_tStates.m_fMinIdleTime)
                {
                    // 이동 상태로 변경
                    SetAnimation(AI_ACTION.WALK);
                }
                break;

            case AI_ACTION.WALK:
                this.transform.position += m_vDirection * m_tStates.m_fMovePerSec;
                if (Vector3.Distance(this.transform.position, m_tStates.m_vIntersection[m_nCurrentIntersection]) < 0.1f)
                {
                    SetDestination(false);
                }

                m_fAnimateTime += Time.deltaTime;
                if (m_fAnimateTime >= 1.0f)
                {
                    m_fAnimateTime = 0.0f;

                    if (Random.Range(0.1f, 1.0f) >= 1.0f - m_tStates.m_fChangeIdlePerSec)
                    {
                        SetAnimation(AI_ACTION.IDLE);
                    }
                }
                break;

            case AI_ACTION.WORK1:
                m_fAnimateTime += Time.deltaTime;
                if (m_fAnimateTime >= m_tStates.m_fMinWorkTime)
                {
                    SetDestination(true);

                    // 이동 상태로 변경
                    SetAnimation(AI_ACTION.WALK);
                }
                break;
        }
	}

    void SetDestination(bool bWorkCheckSkip)
    {
        Quaternion quaternion;

        // 현재 위치를 도착 지점에 보간
        this.transform.position = m_tStates.m_vIntersection[m_nCurrentIntersection];

        // 도착 지점이 작업 지점이라면 작업을 수행
        if (!bWorkCheckSkip && m_nCurrentIntersection == m_tStates.m_nWorkSection)
        {
            m_vDirection = (m_tStates.m_pWorkSection.transform.position - this.transform.position).normalized;
            quaternion = Quaternion.LookRotation(m_vDirection);
            transform.rotation = quaternion;

            SetAnimation(AI_ACTION.WORK1);
            
            return;
        }
        
        // 다음 목표 지점을 지정
        m_nCurrentIntersection++;

        if (m_nCurrentIntersection >= m_tStates.m_vIntersection.Length) m_nCurrentIntersection = 0;

        // 목표을 향하도록 회전
        m_vDirection = (m_tStates.m_vIntersection[m_nCurrentIntersection] - this.transform.position).normalized;
        quaternion = Quaternion.LookRotation(m_vDirection);
        transform.rotation = quaternion;
    }

    void SetAnimation(AI_ACTION action)
    {
        m_tStates.m_nAction = action;
        m_fAnimateTime = 0.0f;

        switch (m_tStates.m_nAction)
        {
            case AI_ACTION.IDLE:
                GetComponent<Animation>().Play("IDLE");
                break;

            case AI_ACTION.WALK:
                GetComponent<Animation>().Play("WALK");
                break;

            case AI_ACTION.WORK1:
                GetComponent<Animation>().Play("WORK1");
                break;

            case AI_ACTION.WORK2:
                GetComponent<Animation>().Play("WORK2");
                break;
        }
    }
}
