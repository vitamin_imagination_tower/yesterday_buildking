﻿using UnityEngine;
using System.Collections;

public enum BuffAI_ACTION
{
    FLY,
    BUFF0,
    BUFF1,
    BUFF2,
    BUFF3
}

[System.Serializable]
public struct FLY_AI_States
{
    public BuffAI_ACTION m_nAction;
    public Vector3[] m_vIntersection;

    public GameObject m_pWorkSection_0;
    public GameObject m_pWorkSection_1;
    public GameObject m_pWorkSection_2;
    public GameObject m_pWorkSection_3;

    public int m_nWorkSection_0;
    public int m_nWorkSection_1;
    public int m_nWorkSection_2;
    public int m_nWorkSection_3;

    public Color m_vColor;
    public float m_fMinIdleTime;
    public float m_fMovePerSec;
    public float m_fChangeIdlePerSec;   
}

public class CBuffAI : MonoBehaviour
{
    public FLY_AI_States m_tStates;
    protected Vector3 m_vDirection;
    protected int m_nCurrentIntersection;
    protected float m_fAnimateTime;

    public GameObject BuffEffect;
    float RotationSpeed = 3;
    float OrbitDegrees = 3;

    void Start()
    {
        SetAnimation(m_tStates.m_nAction);
        SetDestination(true);
    }

    void Update()
    {
        int index;
        Vector3 forward;

        for (index = 0; index < m_tStates.m_vIntersection.Length - 1; index++)
        {
            forward = (m_tStates.m_vIntersection[index + 1] - m_tStates.m_vIntersection[index]);
            Debug.DrawRay(m_tStates.m_vIntersection[index] + (Vector3.up * 0.5f), forward, m_tStates.m_vColor);
        }

        forward = (m_tStates.m_vIntersection[0] - m_tStates.m_vIntersection[index]);
        Debug.DrawRay(m_tStates.m_vIntersection[index] + (Vector3.up * 0.5f), forward, m_tStates.m_vColor);


        switch (m_tStates.m_nAction)
        {
            case BuffAI_ACTION.BUFF0:
                m_fAnimateTime += Time.deltaTime;
                BuffEffect.SetActive(true);

                transform.Rotate(Vector3.up, RotationSpeed * Time.deltaTime);
                transform.RotateAround(m_tStates.m_pWorkSection_0.transform.position, Vector3.up, OrbitDegrees);

                if (m_fAnimateTime >= m_tStates.m_fMinIdleTime)
                {
                    SetDestination(true);
                    // 이동 상태로 변경
                    SetAnimation(BuffAI_ACTION.FLY);                    
                }
                break;

            case BuffAI_ACTION.BUFF1:
                m_fAnimateTime += Time.deltaTime;
                BuffEffect.SetActive(true);

                transform.Rotate(Vector3.up, RotationSpeed * Time.deltaTime);
                transform.RotateAround(m_tStates.m_pWorkSection_1.transform.position, Vector3.up, OrbitDegrees);

                if (m_fAnimateTime >= m_tStates.m_fMinIdleTime)
                {
                    SetDestination(true);
                    // 이동 상태로 변경
                    SetAnimation(BuffAI_ACTION.FLY);
                }
                break;

            case BuffAI_ACTION.BUFF2:
                m_fAnimateTime += Time.deltaTime;
                BuffEffect.SetActive(true);

                transform.Rotate(Vector3.up, RotationSpeed * Time.deltaTime);
                transform.RotateAround(m_tStates.m_pWorkSection_2.transform.position, Vector3.up, OrbitDegrees);

                if (m_fAnimateTime >= m_tStates.m_fMinIdleTime)
                {
                    SetDestination(true);
                    // 이동 상태로 변경
                    SetAnimation(BuffAI_ACTION.FLY);
                }
                break;

            case BuffAI_ACTION.BUFF3:
                m_fAnimateTime += Time.deltaTime;
                BuffEffect.SetActive(true);

                transform.Rotate(Vector3.up, RotationSpeed * Time.deltaTime);
                transform.RotateAround(m_tStates.m_pWorkSection_3.transform.position, Vector3.up, OrbitDegrees);

                if (m_fAnimateTime >= m_tStates.m_fMinIdleTime)
                {
                    SetDestination(true);
                    // 이동 상태로 변경
                    SetAnimation(BuffAI_ACTION.FLY);
                }
                break;


            case BuffAI_ACTION.FLY:
                BuffEffect.SetActive(false);
                this.transform.position += m_vDirection * m_tStates.m_fMovePerSec;
                
                if (Vector3.Distance(this.transform.position, m_tStates.m_vIntersection[m_nCurrentIntersection]) < 0.1f)
                {
                    SetDestination(false);
                }

                m_fAnimateTime += Time.deltaTime;
                if (m_fAnimateTime >= 1.0f)
                {
                    m_fAnimateTime = 0.0f;
                }
                break;
        }

    }

    void SetDestination(bool bWorkCheckSkip)
    {
        Quaternion quaternion;

        // 현재 위치를 도착 지점에 보간
        this.transform.position = m_tStates.m_vIntersection[m_nCurrentIntersection];

        // 도착 지점이 작업 지점이라면 작업을 수행
        if (!bWorkCheckSkip && m_nCurrentIntersection == m_tStates.m_nWorkSection_0) 
        {
            m_vDirection = (m_tStates.m_pWorkSection_0.transform.position - this.transform.position).normalized;
            quaternion = Quaternion.LookRotation(m_vDirection);
            transform.rotation = quaternion;

            SetAnimation(BuffAI_ACTION.BUFF0);

            return;
        }

        if (!bWorkCheckSkip && m_nCurrentIntersection == m_tStates.m_nWorkSection_1)
        {
            m_vDirection = (m_tStates.m_pWorkSection_1.transform.position - this.transform.position).normalized;
            quaternion = Quaternion.LookRotation(m_vDirection);
            transform.rotation = quaternion;

            SetAnimation(BuffAI_ACTION.BUFF1);

            return;
        }

        if (!bWorkCheckSkip && m_nCurrentIntersection == m_tStates.m_nWorkSection_2)
        {
            m_vDirection = (m_tStates.m_pWorkSection_2.transform.position - this.transform.position).normalized;
            quaternion = Quaternion.LookRotation(m_vDirection);
            transform.rotation = quaternion;

            SetAnimation(BuffAI_ACTION.BUFF2);

            return;
        }

        if (!bWorkCheckSkip && m_nCurrentIntersection == m_tStates.m_nWorkSection_3)
        {
            m_vDirection = (m_tStates.m_pWorkSection_3.transform.position - this.transform.position).normalized;
            quaternion = Quaternion.LookRotation(m_vDirection);
            transform.rotation = quaternion;

            SetAnimation(BuffAI_ACTION.BUFF3);

            return;
        }


        // 다음 목표 지점을 지정
        m_nCurrentIntersection++;

        if (m_nCurrentIntersection >= m_tStates.m_vIntersection.Length) m_nCurrentIntersection = 0;

        // 목표을 향하도록 회전
        m_vDirection = (m_tStates.m_vIntersection[m_nCurrentIntersection] - this.transform.position).normalized;
        quaternion = Quaternion.LookRotation(m_vDirection);
        transform.rotation = quaternion;
    }

    void SetAnimation(BuffAI_ACTION action)
    {
        m_tStates.m_nAction = action;
        m_fAnimateTime = 0.0f;

        switch (m_tStates.m_nAction)
        {
            case BuffAI_ACTION.BUFF0:
                GetComponent<Animation>().Play("Fly");
                break;

            case BuffAI_ACTION.BUFF1:
                GetComponent<Animation>().Play("Fly");
                break;

            case BuffAI_ACTION.BUFF2:
                GetComponent<Animation>().Play("Fly");
                break;

            case BuffAI_ACTION.BUFF3:
                GetComponent<Animation>().Play("Fly");
                break;

            case BuffAI_ACTION.FLY:
                GetComponent<Animation>().Play("Fly");
                break;
        }
    }
}
