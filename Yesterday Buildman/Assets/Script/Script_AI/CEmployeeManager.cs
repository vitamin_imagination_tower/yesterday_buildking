﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct EmployeeHandler
{
    public GameObject employee;
    public int level;
    public ulong[] cost;
}

public class CEmployeeManager : MonoBehaviour
{
    public EmployeeHandler[] m_tEmployeeHandler = new EmployeeHandler[ConstantsRW.OBJ_ARRAY_SIZE];
    public GameObject m_pPlayerManager;

    public void SetEmployee(ItemInfo[] employee)
    {
        for (int index = 0; index < employee.Length; index++)
        {
            m_tEmployeeHandler[index].level = employee[index].itemLevel;
            if (m_tEmployeeHandler[index].level > 0)
            {
                m_tEmployeeHandler[index].employee.SetActive(true);
                m_pPlayerManager.GetComponent<CPlayer>().SetEmployeeLevel(index, m_tEmployeeHandler[index].level);
            }
        }
    }
    
    public void UpgradeEmployee(int key)
    {
        int level = m_tEmployeeHandler[key].level;
        ulong cost = m_tEmployeeHandler[key].cost[level];

        if (level < 10)
        {
            if (m_pPlayerManager.GetComponent<CPlayer>().GetGold() >= cost)
            {
                m_pPlayerManager.GetComponent<CPlayer>().DelGold(cost);
                m_tEmployeeHandler[key].level++;
                m_pPlayerManager.GetComponent<CPlayer>().SetEmployeeLevel(key, m_tEmployeeHandler[key].level);

                if(m_tEmployeeHandler[key].level == 1)
                {
                    m_tEmployeeHandler[key].employee.SetActive(true);
                }
            }
        }
    }

    public void SetEqiupment(ItemInfo[] employee)
    {
        for (int index = 0; index < employee.Length; index++)
        {
            m_tEmployeeHandler[index].level = employee[index].itemLevel;
            if (m_tEmployeeHandler[index].level > 0)
            {
                m_tEmployeeHandler[index].employee.SetActive(true);
                m_pPlayerManager.GetComponent<CPlayer>().SetEquipmentLevel(index, m_tEmployeeHandler[index].level);
            }
        }
    }

    public void UpgradeEquipment(int key)
    {
        int level = m_tEmployeeHandler[key].level;
        ulong cost = m_tEmployeeHandler[key].cost[level];

        if (level < 10)
        {
            if (m_pPlayerManager.GetComponent<CPlayer>().GetGold() >= cost)
            {
                m_pPlayerManager.GetComponent<CPlayer>().DelGold(cost);
                m_tEmployeeHandler[key].level++;
                m_pPlayerManager.GetComponent<CPlayer>().SetEquipmentLevel(key, m_tEmployeeHandler[key].level);

                if (m_tEmployeeHandler[key].level == 1)
                {
                    m_tEmployeeHandler[key].employee.SetActive(true);
                }
            }
        }
    }
}
