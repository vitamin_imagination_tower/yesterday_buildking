﻿using UnityEngine;
using System.Collections;
using QuickPool;

public class EffectTest : MonoBehaviour {

    public GameObject Effect;


    void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag == "Plane")
        {
            Effect.Spawn(transform.position, Quaternion.identity);
        }

    }
}
