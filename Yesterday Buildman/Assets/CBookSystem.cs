﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CBookSystem : MonoBehaviour
{
    public RawImage BuildImage;
    public Text T_LegacyName;
    public Text T_LV;
    public Text T_Type;
    public Text T_ClearCount;
    public Text T_FirstClear;
    public Text T_SumMoney;

    public Texture Build_A_0;
    public Texture Build_A_1;
    public Texture Build_A_2;
    public Texture Build_A_3;
    public Texture Build_A_4;


    // 레거시 건물 레벨 데이터
    int Build_N_0_LV;
    int Build_N_1_LV;
    int Build_N_2_LV;
    int Build_N_3_LV;
    int Build_N_4_LV;

    // 레거시 건물 레벨 데이터
    int Legacy_N_0_LV;
    int Legacy_N_1_LV;
    int Legacy_N_2_LV;

    // 일반 건물 클리어 횟수
    int Build_N_0_ClearCount;
    int Build_N_1_ClearCount;
    int Build_N_2_ClearCount;
    int Build_N_3_ClearCount;
    int Build_N_4_ClearCount;

    // 레거시건물 클리어 횟수
    int Legacy_N_0_ClearCount;
    int Legacy_N_1_ClearCount;
    int Legacy_N_2_ClearCount;


    // 일반 건물 지어 번 총 금액
    int Build_N_0_SumMoney;
    int Build_N_1_SumMoney;
    int Build_N_2_SumMoney;
    int Build_N_3_SumMoney;
    int Build_N_4_SumMoney;

    // 레거시 건물 지어 번 총 금액
    int Legacy_N_0_SumMoney;
    int Legacy_N_1_SumMoney;
    int Legacy_N_2_SumMoney;
    

    public void BuildInfo(int BuildNumber)
    {
        switch (BuildNumber)
        {
            case 0:
                BuildImage.texture = Build_A_0;
                T_LegacyName.text = "아주 작은 모래성";
                T_LV.text = "" + Build_N_0_LV;
                T_Type.text = "모래";
                T_ClearCount.text = "" + Build_N_0_ClearCount;
                T_FirstClear.text = "";
                T_SumMoney.text = "" + Build_N_0_SumMoney;
                break;
            case 1:
                BuildImage.texture = Build_A_1;
                T_LegacyName.text = "작은 모래성";
                T_LV.text = "" + Build_N_1_LV;
                T_Type.text = "모래";
                T_ClearCount.text = "" + Build_N_1_ClearCount;
                T_FirstClear.text = "";
                T_SumMoney.text = "" + Build_N_1_SumMoney;
                break;
            case 2:
                BuildImage.texture = Build_A_2;
                T_LegacyName.text = "적당한 모래성";
                T_LV.text = "" + Build_N_2_LV;
                T_Type.text = "모래";
                T_ClearCount.text = "" + Build_N_2_ClearCount;
                T_FirstClear.text = "";
                T_SumMoney.text = "" + Build_N_2_SumMoney;
                break;
            case 3:
                BuildImage.texture = Build_A_3;
                T_LegacyName.text = "대형 모래성";
                T_LV.text = "" + Build_N_3_LV;
                T_Type.text = "모래";
                T_ClearCount.text = "" + Build_N_3_ClearCount;
                T_FirstClear.text = "";
                T_SumMoney.text = "" + Build_N_3_SumMoney;
                break;
            case 4:
                BuildImage.texture = Build_A_4;
                T_LegacyName.text = "초대형 모래성";
                T_LV.text = "" + Build_N_4_LV;
                T_Type.text = "모래";
                T_ClearCount.text = "" + Build_N_4_ClearCount;
                T_FirstClear.text = "";
                T_SumMoney.text = "" + Build_N_4_SumMoney;
                break;
            
        }
    }

    public void LegacyInfo(int LegacyNumber)
    {
        switch(LegacyNumber)
        {
            case 0:
                T_LegacyName.text = "피라미드";
                T_LV.text = "" + Legacy_N_0_LV;
                T_Type.text = "모래";
                T_ClearCount.text = "" + Legacy_N_0_ClearCount;
                T_FirstClear.text = "";
                T_SumMoney.text = "" + Legacy_N_0_SumMoney;
                break;
            case 1:
                T_LegacyName.text = "피사의 사탑";
                T_LV.text = "" + Legacy_N_1_LV;
                T_Type.text = "돌";
                T_ClearCount.text = "" + Legacy_N_1_ClearCount;
                T_FirstClear.text = "";
                T_SumMoney.text = "" + Legacy_N_1_SumMoney;
                break;
            case 2:
                T_LegacyName.text = "경복궁";
                T_LV.text = "" + Legacy_N_2_LV;
                T_Type.text = "주택";
                T_ClearCount.text = "" + Legacy_N_2_ClearCount;
                T_FirstClear.text = "";
                T_SumMoney.text = "" + Legacy_N_2_SumMoney;
                break;
        }
    }
}
